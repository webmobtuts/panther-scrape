<div>
    <h2 class="text-3xl font-bold">Enter url to scrape</h2>
    <form method="post" class="mt-4" wire:submit.prevent="scrape">
        @if(session()->has("success"))
            <div class="bg-green-600 text-white rounded p-4 flex justify-start items-center">
                <div>
                    <svg class="w-10" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <span>
                    {{ session("success") }}
                </span>
            </div>
        @endif
        <div class="flex flex-col mb-4">
            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest">Url</label>
            <input type="text" wire:model.debounce.500ms="url" class="border border-gray-400 py-2 px-3 text-grey-darkest placeholder-gray-500" name="url" id="url" placeholder="Url..." />
            @error('url') <span class="text-red-700">{{ $message }}</span> @enderror
            <button type="submit" wire:loading.attr="disabled" class="block bg-red-400 hover:bg-teal-dark text-lg mx-auto p-4 rounded mt-3">Add to queue</button>
        </div>
    </form>
    @if($startScrape)
        <p>Products will be loaded shorlty! don't close the page...</p>
    @endif
    <div>
        @if(count($products) > 0) <div class="flex items-center justify-left">Found <span class="rounded-full h-10 w-10 flex items-center justify-center bg-blue-500">{{ count($products) }}</span> Products</div> @endif
        @foreach($products as $product)
            <livewire:product :product="$product" :key="$product['id']" />
        @endforeach
    </div>
</div>

@push('scripts')
    <script>
           window.Echo.channel('crawler')
            .listen('ProductReceived', (e) => {
                //console.log(e.product);
            });
    </script>
@endpush
