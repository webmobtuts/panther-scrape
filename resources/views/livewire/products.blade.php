<div>
    @foreach($products as $product)
        <livewire:product :product="$product" :key="$product['id']" />
    @endforeach

    {{ $products->links() }}
</div>


