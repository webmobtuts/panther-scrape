<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Artisan;
use Livewire\Component;

class ScraperForm extends Component
{
    public $url;

    public $products = [];

    public $startScrape = false;

    protected $rules = [
        'url' => 'required|url'
    ];

    protected $listeners = ['echo:crawler,ProductReceived' => 'onProductFetch'
        ];

    public function scrape()
    {
        $this->validate();

        try {
            Artisan::queue("Url:Crawl", ["url" => $this->url])
                ->onConnection('database')->onQueue('processing');

            $this->url = "";

            $this->startScrape = true;

            session()->flash("success", "Url Added Successfully to queue and it will be processed shortly!");

        } catch (\Exception $ex) {
            dd($ex);
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function onProductFetch($data)
    {
        array_unshift($this->products, $data['product']);
    }

    public function render()
    {
        return view('livewire.scraper-form');
    }
}
